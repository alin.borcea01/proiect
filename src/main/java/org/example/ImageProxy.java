package org.example;

import java.awt.*;

public class ImageProxy implements Picture {
    private String url;
    private Dimension dim;
    private Image realImage;

    public ImageProxy(String url, Dimension dim) {
        this.url = url;
        this.dim = dim;
    }

    private Image loadImage() {
        if (realImage == null) {
            realImage = new Image(url);
        }

        return realImage;
    }

    @Override
    public String url() {
        return url;
    }

    @Override
    public Dimension dim() {
        return dim;
    }

    @Override
    public ImageContent content() {
        return realImage.content();
    }
}
