package org.example;

public class Paragraph implements Element {
    private String text;
    private AlignStrategy alignment;

    public Paragraph(String paragraph) {
        this.text = paragraph;
    }

    void setAlignStrategy(AlignStrategy align) {
        alignment = align;
        align.render(this, null);
    }

    public void setText(String text) {
        this.text = text;
    }
    public String getText() {
        return text;
    }

    @Override
    public void print() {
        System.out.println("Paragraph: " + text + "\n");
    }

}
