package org.example;

import java.awt.*;

public class JPGImageLoader implements ImageLoader {
    @Override
    public ImageContent load(String id) {
        return new ImageContent("Id.jpg", new Dimension(id.length(), id.length()));
    }
}
