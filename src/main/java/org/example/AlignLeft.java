package org.example;

import javax.naming.Context;

public class AlignLeft implements AlignStrategy {
    private static final Alignment alignment = Alignment.left;

    @Override
    public void render(Paragraph p, Context c) {
         p.setText("*******" + p.getText());
    }
}
