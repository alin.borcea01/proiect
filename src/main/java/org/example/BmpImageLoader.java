package org.example;

import java.awt.*;

public class BmpImageLoader implements ImageLoader {
    @Override
    public ImageContent load(String id) {
        return new ImageContent("Id.bmp", new Dimension(id.length(), id.length()));
    }
}
