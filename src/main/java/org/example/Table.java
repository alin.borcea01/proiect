package org.example;

public class Table implements Element {
    private final String something;

    public Table(String something) {
        this.something = something;
    }

    @Override
    public void print() {
        System.out.println("Table with title: " + something + "\n");
    }
}
