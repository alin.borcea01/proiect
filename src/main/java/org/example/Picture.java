package org.example;

import java.awt.*;

public interface Picture {
    String url();
    Dimension dim();
    ImageContent content();
}
