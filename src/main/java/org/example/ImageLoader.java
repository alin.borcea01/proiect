package org.example;

public interface ImageLoader {
    ImageContent load(String id);
}
