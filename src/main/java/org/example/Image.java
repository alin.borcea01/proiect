package org.example;

import java.awt.*;
import java.util.concurrent.TimeUnit;

public class Image implements Element, Picture {
    private final String url;
    private final ImageContent content;

    public Image(String url) {
        this.url = url;
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        content = ImageLoaderFactory.create(url);
    }

    @Override
    public void print() {
        System.out.println("Image with name: " + url + "\n");
    }

    @Override
    public String url() {
        return url + " " + (content != null ? content : "");
    }

    @Override
    public Dimension dim() {
        return content.getDim();
    }

    @Override
    public ImageContent content() {
        return content;
    }
}
