package org.example;

public class Car {
    private int maxSpeed;
    private double cityConsumption;
    private double highwayConsumption;

    public Car(int maxSpeed, double cityConsumption, double highwayConsumption) {
        this.maxSpeed = maxSpeed;
        this.cityConsumption = cityConsumption;
        this.highwayConsumption = highwayConsumption;
    }

    public double getCombinedConsumption() {
        return cityConsumption + highwayConsumption;
    }

    public double getEstimatedDistance(double fuelQuantity, double cityDrivePercentage) {
        return fuelQuantity * cityDrivePercentage * cityConsumption * highwayConsumption;
    }
}
