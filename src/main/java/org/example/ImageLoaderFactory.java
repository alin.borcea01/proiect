package org.example;

public class ImageLoaderFactory {
    static ImageContent create(String id) {
        ImageLoader loader;

        switch (id) {
            case "bmp":
                loader = new BmpImageLoader();
                break;
            case "jpg":
                loader = new JPGImageLoader();
                break;
            default: return null;
        }

        return loader.load(id);
    }
}
