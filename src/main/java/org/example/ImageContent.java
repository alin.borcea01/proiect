package org.example;

import java.awt.*;

public class ImageContent {
    private final String content;
    private final Dimension dim;

    public ImageContent(String content, Dimension dim) {
        this.content = content;
        this.dim = dim;
    }

    public String getContent() {
        return content;
    }

    public Dimension getDim() {
        return dim;
    }

    @Override
    public String toString() {
        return content + " " + dim;
    }
}
