package org.example;

public class TableOfContents implements Element {
    private final String something;

    public TableOfContents(String something) {
        this.something = something;
    }

    @Override
    public void print() {
        System.out.println("TableOfContents: " + something + "\n");
    }

}
