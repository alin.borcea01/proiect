package org.example;

import java.util.ArrayList;
import java.util.List;

public class Section implements Element {
    protected final String title;
    private final List<Element> elements;

    public Section(String title) {
        this.title = title;
        elements = new ArrayList<>();
    }

    @Override
    public void print() {
        System.out.println(title);
        for (Element e : elements) {
            e.print();
        }
    }

    @Override
    public void add(Element e) {
        if (e == this || e.hasParent()) {
            throw new UnsupportedOperationException();
        }

        if (elements.contains(e)) {
            throw new UnsupportedOperationException();
        }
        elements.add(e);
        e.setParent(this);
    }

    @Override
    public void remove(Element e) {
        elements.remove(e);
    }

    @Override
    public Element get(int index) {
        return elements.get(index);
    }
}
