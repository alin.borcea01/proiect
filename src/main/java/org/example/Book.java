package org.example;

import java.util.ArrayList;
import java.util.List;

public class Book extends Section {
    private final List<Author> authors;

    public Book(String title) {
        super(title);
        authors = new ArrayList<>();
    }

    void addAuthor(Author author) {
        authors.add(author);
    }

    void addContent(Element e) {
        super.add(e);
    }

    @Override
    public void print() {
        System.out.println("Book: " + super.title);
        System.out.println("Authors\n");
        for (Author a : authors) {
            a.print();
        }
        super.print();
    }

}
